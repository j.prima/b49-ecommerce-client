import React, { useState, useEffect } from "react";
import Swal from "sweetalert2";
import Stripe from "./forms/Stripe";
import { URL } from "../config";

const Cart = () => {
	const [cartItems, setCartItems] = useState([]);

	let total;
	useEffect( () => {
		setCartItems(JSON.parse(localStorage.getItem("cartItems")))
	}, [])

	if (cartItems.length) {
		let subTotals = cartItems.map(item => parseInt(item.price)* parseInt(item.quantity))

        total = subTotals.reduce((accumulator, subPerItem) => {
            return accumulator + subPerItem
        })
	}

	const emptyCart = () => {
		let cartItems = []
		localStorage.setItem("cartItems", JSON.stringify(cartItems))
		window.location.href = "/"
	}

	const deleteCartItem = (itemId) => {
		let updatedCartItems = cartItems.filter(item => {
			return item._id !== itemId
		})
		localStorage.setItem("cartItems", JSON.stringify(updatedCartItems))
		window.location.href = "/cart"
	}

	const checkout = () => {
		let orders = JSON.parse(localStorage.getItem("cartItems"))
		fetch(`${URL}/transactions`, {
			method: "POST",
			body: JSON.stringify({orders, total}),
			headers: {
				"Content-Type": "application/json",
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				"icon": "success",
				"title": data.message
			})
			localStorage.setItem("cartItems", JSON.stringify([]))
			setCartItems(JSON.stringify(localStorage.getItem("cartItems")))
		})
	}

	const quantityHandler = (quantity, productId) => {
		let itemToUpdate = cartItems.find(item => item._id === productId)
		itemToUpdate.quantity = parseInt(quantity)
		
		console.log(itemToUpdate.quantity)
		
		let updatedCart = cartItems.map(item => {
			return item._id === productId ? {...itemToUpdate} : item
		})

		localStorage.setItem("cartItems", JSON.stringify(updatedCart))
		setCartItems(JSON.parse(localStorage.getItem("cartItems")))
	}

	return (
		<div className="col-md-10 mx-auto">
			<h2 className="my-2">Cart</h2>
			{
				cartItems.length ? 
				<table className="table table-striped table-hover">
					<thead>
						<tr>
							<th>Item</th>
	                        <th>Price</th>
	                        <th>Quantity</th>
	                        <th>Subtotal</th>
	                        <th>Actions</th>
						</tr>
					</thead>

					<tbody>
						{cartItems.map(item => (
							<tr key={item._id}>
								<td>{item.name}</td>
								<td>{item.description}</td>
								<td>
									<input 
										type="number" 
										onChange={(e) => quantityHandler(e.target.value, item._id)} 
										value={item.quantity} 
									/>
								</td>
								<td>{item.quantity * item.price}</td>
								<td>
									<button className="btn btn-danger" onClick={() => deleteCartItem(item._id)}>
										Remove
									</button>
								</td>
							</tr>	
						))}
						<tr>
							<td colSpan="3">Total</td>
							<td colSpan="2">{total}</td>
						</tr>
						<tr>
							<td colSpan="5">
								<button className="btn btn-danger mr-3" onClick={() => emptyCart()}>Empty Cart</button>
								<button className="btn btn-success mr-3" onClick={checkout}>Check COD</button>
								<Stripe amount={total} email={"johnprima071492@gmail.com"} />
							</td>
						</tr>
					</tbody>
				</table>
				:
				<h3>Empty Cart</h3>
			}
		</div>
	)
}



export default Cart;