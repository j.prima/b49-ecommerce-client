import React, { useState, useEffect } from "react";
import Swal from "sweetalert2";
import { URL } from "../../config";

const AddProduct = () => {
	const [formData, setFormData] = useState({});
	const [categories, setCategories] = useState([]);

	useEffect( () => {
		fetch(`${URL}/categories`, {
			headers: {
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			setCategories(data)
		})
	}, [])

	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	}

	const handleFile = (e) => {
		// console.log(e.target.files[0])
		// if (e.target.files[0] != null) {
			setFormData({
				...formData,
				image: e.target.files[0]
			})
		// }
	}

	const onSubmitHandler = (e) => {
		e.preventDefault()

		const product = new FormData()
		product.append('name', formData.name)
		product.append('description', formData.description)
		product.append('price', formData.price)
		product.append('categoryId', formData.categoryId)
		product.append('image', formData.image)

		fetch(`${URL}/products`, {
			method: "POST",
			body: product,
			headers: {
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.status === 200) {
				Swal.fire({
					icon: "success",
					text: data.message
				})
				window.location.href = "/"
			} else {
				Swal.fire({
					icon: "error",
					text: "Please check your inputs"
				})
			}
		})
	}

	return (
		<form className="mx-auto mt-5 col-sm-6" onSubmit={onSubmitHandler} encType="multipart/form-data">
			<div className="form-group">
				<label>Name</label>
				<input 
					type="text"
					name="name"
					className="form-control"
					onChange={onChangeHandler}
				/>
			</div>
			<div className="form-group">
				<label>Description</label>
				<input 
					type="text"
					name="description"
					className="form-control"
					onChange={onChangeHandler}
				/>
			</div>
			<div className="form-group">
				<label>Price</label>
				<input 
					type="number"
					name="price"
					className="form-control"
					onChange={onChangeHandler}
				/>
			</div>
			<div className="form-group">
				<label>Image</label>
				<input 
					type="file"
					name="image"
					className="form-control" 
					onChange={handleFile}
				/>
			</div>
			<div className="form-group">
				<label>Category</label>
				<select 
					className="form-control mb-3" 
					name="categoryId" 
					onChange={onChangeHandler}
				>
					<option disabled selected>
						Select Category
					</option>
					{categories.map(category => (
						<option value={category._id} key={category._id}>
							{category.name}
						</option>
					))}
				</select>
			</div>
			<button className="btn btn-info">Add Product</button>
		</form>
	)
}

export default AddProduct;