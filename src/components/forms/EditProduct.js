import React, { useState, useEffect } from "react";
import Swal from "sweetalert2";
import { URL } from "../../config";

const EditProduct = ({setEditing, product}) => {
	const [formData, setFormData] = useState({...product});
	const [categories, setCategories] = useState([]);

	useEffect( () => {
		fetch(`${URL}/categories`, {
			headers: {
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			setCategories(data)
		})
	}, [])

	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	}

	const handleFile = (e) => {
		setFormData({
			...formData,
			image: e.target.files[0]
		})
	}

	const onSubmitHandler = (e) => {
		e.preventDefault()
		console.log(formData)

		const updatedProduct = new FormData()
		updatedProduct.append('name', formData.name)
		updatedProduct.append('description', formData.description)
		updatedProduct.append('price', formData.price)
		updatedProduct.append('categoryId', formData.categoryId)
		if (typeof formData.image === 'object') {
			updatedProduct.append('image', formData.image)	
		}

		fetch(`${URL}/products/${product._id}`, {
			method: "PUT",
			body: updatedProduct,
			headers: {
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if(data.status === 200) {
				Swal.fire({
					icon: "success",
					text: data.message
				})
				setEditing(false)
			}
		})
	}

	return (
		<form className="mx-auto mt-5 col-sm-6" onSubmit={onSubmitHandler} encType="multipart/form-data">
			<div className="form-group">
				<label>Name</label>
				<input 
					type="text"
					name="name"
					className="form-control"
					onChange={onChangeHandler}
					value={formData.name}
				/>
			</div>
			<div className="form-group">
				<label>Description</label>
				<input 
					type="text"
					name="description"
					className="form-control"
					onChange={onChangeHandler}
					value={formData.description}
				/>
			</div>
			<div className="form-group">
				<label>Price</label>
				<input 
					type="number"
					name="price"
					className="form-control"
					onChange={onChangeHandler}
					value={formData.price}
				/>
			</div>
			<div className="form-group">
				<label>Image</label>
				<input 
					type="file"
					name="image"
					className="form-control" 
					onChange={handleFile}
				/>
			</div>
			<div className="form-group">
				<label>Category</label>
				<select 
					className="form-control mb-3" 
					name="categoryId" 
					onChange={onChangeHandler}
				>
					<option disabled>
						Select Category
					</option>
					{categories.map(category => (
						product.categoryId === category._id ? 
							<option value={category._id} key={category._id} defaultValue>
								{category.name}
							</option> :
							<option value={category._id} key={category._id}>
								{category.name}
							</option>
						
					))}
				</select>
			</div>
			<button className="btn btn-info mx-2" onClick={onSubmitHandler}>Edit Product</button>
			<button className="btn btn-info mx-2" onClick={() => setEditing(false)}>Cancel</button>
		</form>
	)
}

export default EditProduct;