import React, { useState } from "react";
import Swal from "sweetalert2";
import { getReqAtts } from "./Constants";
import { URL } from "../../config";

const Register = () => {
	const [formData, setFormData] = useState({});

	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	};

	const onSubmitHandler = (e) => {
		e.preventDefault()
		fetch(`${URL}/users`, getReqAtts(formData))
		.then(res => res.json())
		.then(data => {
			if(data.status === 200) {
				Swal.fire({
					icon: "success",
					text: data.message
				})
			} else {
				Swal.fire({
					icon: "error",
					text: data.message,
					timer: 3000,
					showConfirmButton: false
				})
			}
		})
	};

	return (
		<div className="container mt-5">
			<div className="form-group">
				<label htmlFor="fullname">Fullname</label>
				<input 
					className="form-control" 
					type="text" 
					id="fullname" 
					name="fullname" 
					onChange={onChangeHandler}
				/>
			</div>
			<div className="form-group">
				<label htmlFor="username">Username</label>
				<input 
					className="form-control" 
					type="text" 
					id="username" 
					name="username" 
					onChange={onChangeHandler}
				/>
			</div>
			<div className="form-group">
				<label htmlFor="password">Password</label>
				<input 
					className="form-control" 
					type="password" 
					id="password" 
					name="password" 
					onChange={onChangeHandler}
				/>
			</div>
			<div className="form-group">
				<label htmlFor="password2">Confirm Password</label>
				<input 
					className="form-control" 
					type="password" 
					id="password2" 
					name="password2" 
					onChange={onChangeHandler}
				/>
			</div>
			<button className="btn btn-success" onClick={onSubmitHandler}>
				Register
			</button>
		</div>
	)
}

export default Register;