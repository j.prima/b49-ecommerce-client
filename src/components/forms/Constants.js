const test = "Test success!";

const getReqAtts = (data) => {
	return (
		{
			method: "POST",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json"
			}		
		}
	)
};	

export { test, getReqAtts };