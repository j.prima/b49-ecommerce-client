import React, { useState } from "react";
import Swal from "sweetalert2";
import { getReqAtts } from "./Constants";
import { URL } from "../../config";

const Login = () => {
	const [formData, setFormData] = useState({});

	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	}

	const onSubmitHandler = (e) => {
		fetch(`${URL}/users/login`, {
			method: "POST",
			body: JSON.stringify(formData),
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.auth) {
				Swal.fire({
					icon: "success",
					title: data.message,
					showConfirmButton: false,
					timer: 3000
				})

				let cartItems = [];
				localStorage.setItem("user", JSON.stringify(data.user))
				localStorage.setItem("token", data.token)
				localStorage.setItem("cartItems", JSON.stringify(cartItems))

				window.location.href="/"
			} else {
				Swal.fire({
					icon: "error",
					title: data.message,
					showConfirmButton: false,
					timer: 3000
				})
			}
		})
	}

	return (
		<div className="container">
			<form className="mx-auto mt-5 col-sm6" onSubmit={onSubmitHandler}>
				<div className="form-group">
					<label htmlFor="username">Username</label>
					<input 
						className="form-control"
						type="text" 
						id="username" 
						name="username" 
						onChange={onChangeHandler} 
					/>
				</div>
				<div className="form-group">
					<label htmlFor="password">Password</label>
					<input 
						className="form-control"
						type="password" 
						id="password" 
						name="password" 
						onChange={onChangeHandler} 
					/>
				</div>
				<button className="btn btn-success">
					Login
				</button>
			</form>
		</div>
	)
}

export default Login;