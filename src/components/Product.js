import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";
import EditPost from "./forms/EditProduct";
import { URL } from "../config";

const Product = () => {
	let {id} = useParams();
	const [product, setProduct] = useState({});
	const [editing, setEditing] = useState(false);

	useEffect( () => {
		fetch(`${URL}/products/${id}`)
		.then(res => res.json())
		.then(data => {
			setProduct(data)
		})
	}, [product]);

	const deleteProduct = () => {
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.value) {
				fetch(`${URL}/products/${id}`, {
					method: "DELETE",
					headers: {
						"x-auth-token": localStorage.getItem("token")
					}
				})
				.then(res => res.json())
				.then(data => {
					Swal.fire(
						"Deleted",
						"Product has been deleted.",
						"success"
					)

					window.location = "/"
				})
			}
			
		})
	}

	return (
		<div className="container mx-auto mt-5 col-sm-12">
			{editing ? <EditPost setEditing={setEditing} product={product} /> :
				<div className="row mx-auto my-auto">
					<div className="col-sm-5 mr-2">
						<img src={`${URL}/${product.image}`} alt="..." />
					</div>

					<div className="col-sm-5">
						<h3>{product.name}</h3>
						<hr />
						<p>{product.description}</p>
						<p>Price: ${product.price}</p>
						<button type="button" className="btn btn-warning mr-2" onClick={() => setEditing(true)}>Edit</button>
						<button type="button" className="btn btn-danger" onClick={deleteProduct}>Delete</button>
					</div>
				</div>
			}
		</div>
	)
};

export default Product;