import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { URL } from "../config";

const UserTransactions = () => {
	let {userId} = useParams()
	const [transactions, setTransactions] = useState([]);

	useEffect(() => {
		fetch(`${URL}/transactions/${userId}`, {
			headers: {
				"x-auth-token": localStorage.getItem("token")
			}
		})
		.then(res => res.json())
		.then(data => setTransactions(data))
	})

	return(
		<div className="container">
			<h2>My Transactions</h2>
			{
				transactions.length ?
				<table className="table table-striped table-hover">
					<thead className="table table-hover">
						<tr>
							<th>Transaction ID</th>
							<th>Status</th>
							<th>Amount</th>
							<th>Data</th>
						</tr>
					</thead>

					<tbody>
						{transactions.map(transaction => (
							<tr>
								<td>{transaction._id}</td>
								<td>
									{transaction.statusId === "" ?
										"Pending" : 
										(transaction.statusId === "" ? 
											"Completed" : 
											"Cancelled"
										)
									}
								</td>
								<td>{transaction.total}</td>
								<td>{new Date(transaction.dateCreated).toLocalStorage}</td>
							</tr>
						))}
					</tbody>
				</table>
				: <h2>No transactions to show</h2>
			}
		</div>
	)
}

export default UserTransactions;