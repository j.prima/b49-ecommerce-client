import React, { useState } from "react";
import { 
			Container,
			Row,
			Col, 
			Card,
			CardTitle,
			CardImg,
			CardText,
			CardFooter
		} from "reactstrap";
import { Link } from "react-router-dom";
import { URL } from "../config";

const Catalog = ({products}) => {
	const [quantity, setQuantity] = useState(0);

	const addToCart = (e, product) => {
		let cartItems = JSON.parse(localStorage.getItem("cartItems"))
		let isMatched = cartItems.find(item => {
			return item._id === product._id
		})

		if (isMatched) {
			isMatched.quantity += parseInt(e.target.previousElementSibling.value)
		} else {
			cartItems.push({
				...product,
				quantity: parseInt(e.target.previousElementSibling.value)
			})
		}

		localStorage.setItem("cartItems", JSON.stringify(cartItems))
	};

	const showProducts = products.map(product => (
		<Col sm="4" key={product._id}>
			<Card body outline color="info" className="p-3 mb-3">
				<CardTitle>
					<Link className="m-2" to={`/products/${product._id}`}>{product.name}</Link>
				</CardTitle>
				<CardImg src={`${URL}/${product.image}`} height="300px" width="100%" />
				<CardText>{product.description}</CardText>
				<CardText><h6>{product.price}</h6></CardText>
				<CardFooter>
					<input 
						className="form-control w-50 d-inline mr-2" 
						type="number" 
						min="1" 
						name="quantity" 
						onChange={(e)=> setQuantity(e.target.value)}
					/>
					<button className="btn btn-info text-nowrap" onClick={(e) => {
						addToCart(e, product)
						e.target.previousElementSibling.value = ""
					}}>
						Add to Card
					</button> 
				</CardFooter>
			</Card>
		</Col>
	));

	return(
		<Container className="mt-5">
			<h2>All Products</h2>
			<Row>
				{showProducts}
			</Row>
		</Container>
	)
};

export default Catalog;