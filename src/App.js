// Libraries
import React, { useState, useEffect } from 'react';
import { 
  BrowserRouter as Router, Switch, Route 
} from "react-router-dom";
import jwt_decode from "jwt-decode";

// Components
import TopNav from "./components/layouts/Navbar";
import Catalog from "./components/Catalog";
import Register from "./components/forms/Register";
import Login from "./components/forms/Login";
import AddProduct from "./components/forms/AddProduct";
import Product from "./components/Product";
import Cart from "./components/Cart";
import Auth from "./components/Auth";
import UserTransactions from "./components/UserTransactions";
import { URL } from "./config";

function App() {
  const [products, setProducts] = useState([]);
  const [user, setUser] = useState({});
  const [token, setToken] = useState("");

  useEffect( () => {
    fetch(`${URL}/products`)
    .then(res => res.json())
    .then(data => {
      setProducts(data)
    })

    if(token) {
      let decoded = jwt_decode(token)
      let now = new Date()
      if(decoded.exp === now.getTime()) {
        localStorage.clear()
        setUser({})
        setToken("")
      }
    }

    setUser(JSON.parse(localStorage.getItem("user")))
    setToken(localStorage.getItem("user"))
  }, []);

  const logoutHandler = () => {
    localStorage.clear()
    setUser({})
    setToken("")
    window.location.href="/"
  };

  return (
    <Router>
      <TopNav 
        user={user}
        token={token}
        logoutHandler={logoutHandler}
      />
      <Switch>

        <Route exact path="/">
          <Catalog products={products} />
        </Route>

        <Route path="/register">
          <Register />
        </Route>
        
        <Route path="/login">
          <Login />
        </Route>

        <Route path="/cart">
            {user && token && !user.isAdmin ?
              <Cart /> : <Auth />  
            }
        </Route>

        <Route path="/transactions/:userId">
            {user && token && !user.isAdmin ?
              <UserTransactions /> : <Auth />  
            }
        </Route>
        
        {user && token && user.isAdmin ? 
          <Route path="/add-product">
            <AddProduct />
          </Route> : <Auth />
        }

        <Route path="/products/:id">
          <Product />
        </Route>

      </Switch>
    </Router>
  );
}

export default App;
